import {Component} from "@angular/core";
import {routerTransition} from "../../router.animations";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  animations: [routerTransition()]
})
export class ContactComponent {}