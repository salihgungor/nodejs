import {Component, Input} from "@angular/core";
import {Message} from "./message.model";
import {MessageService} from "./message.service";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {
  @Input() public message: Message;

  constructor(private messageService: MessageService) { }

  color = 'yellow';

  onEdit() {
    this.messageService.editMessage(this.message);
  }

  onDelete(){
    this.messageService.deleteMessage(this.message)
        .subscribe(
            result => console.log(result)
        );
  }

  belongsToUser() {
    return localStorage.getItem('userId') == this.message.userId;
  }
}