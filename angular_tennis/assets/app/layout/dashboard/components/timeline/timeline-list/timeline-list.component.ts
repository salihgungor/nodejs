import {Component, OnInit} from "@angular/core";
import {Randevu} from "../../../../randevu/randevu.model";
import {RandevuService} from "../../../../randevu/randevu.service";

@Component({
  selector: 'app-timeline-list',
  templateUrl: './timeline-list.component.html'
})
export class TimelineListComponent implements OnInit {
  randevular: Randevu[];


  constructor(private randevuService: RandevuService) { }

  ngOnInit() {
    this.randevuService.getAllRandevu()
        .subscribe(
            (randevular: Randevu[]) => {
              this.randevular = randevular;
            }
        );
  }
}