import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {
    TimelineComponent,
    NotificationComponent,
    ChatComponent
} from './components';
import { StatModule } from '../../shared';
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {RandevuService} from "../randevu/randevu.service";
import {TimelineListComponent} from "./components/timeline/timeline-list/timeline-list.component";
import {MessageComponent} from "./components/messages/message.component";

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        StatModule,
        PageHeaderModule
    ],
    declarations: [
        DashboardComponent,
        TimelineComponent,
        NotificationComponent,
        ChatComponent,
        TimelineListComponent,
        MessageComponent
    ],
    providers: [
        RandevuService
    ]
})
export class DashboardModule {}
