import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {routerTransition} from "../../../router.animations";
import {RandevuService} from "../randevu.service";
import {Randevu} from "../randevu.model";
import {formatSize} from "@angular/cli/utilities/stats";
import {Router} from "@angular/router";


@Component({
  selector: 'app-randevu-al',
  templateUrl: './randevu-al.component.html',
  animations: [routerTransition()]
})
export class RandevuAlComponent implements OnInit{

  myForm: FormGroup;
  randevu: Randevu;

  constructor(private randevuService: RandevuService, private router: Router) {}

  onSubmit() {
    if (this.isLoggedIn()) {
      // kullanıcı
      const randevu = new Randevu(
          '',
          '',
          this.myForm.value.hour,
          this.myForm.value.date,
          this.myForm.value.explanation);
      this.randevuService.addRandevu(randevu)
          .subscribe(
              data => console.log(data),
              error => console.error(error)
          );
    } else {
      // misafir
      const randevu = new Randevu(
          this.myForm.value.name,
          this.myForm.value.phoneNumber,
          this.myForm.value.hour,
          this.myForm.value.date,
          this.myForm.value.explanation);
      this.randevuService.addGuestRandevu(randevu)
          .subscribe(
              data => console.log(data),
              error => console.error(error)
          );
    }
    this.router.navigateByUrl('/dashboard');
  }

  isLoggedIn() {
    return this.randevuService.isLoggedIn();
  }

  ngOnInit() {
    this.myForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      phoneNumber: new FormControl(null, Validators.required),
      hour: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required),
      explanation: new FormControl(null, Validators.required),
    });
  }

}