import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RandevuAlComponent} from "./randevu-al/randevu-al.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'randevu-al',
    pathMatch: 'full'
  },
  {
    path: 'randevu-al',
    component: RandevuAlComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RandevuRoutingModule {}