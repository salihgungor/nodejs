import {Component} from "@angular/core";
import {RandevuService} from "./randevu.service";
import {routerTransition} from "../../router.animations";

@Component({
  selector: 'app-randevu',
  templateUrl: 'randevu.component.html',
  animations: [routerTransition()]
})
export class RandevuComponent {

  constructor(private randevuService: RandevuService) {}

}
