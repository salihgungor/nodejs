export class Randevu {
  username: string;
  phoneNumber: string;
  hour: string;
  date: string;
  explanation?: string;
  randevuId?: string;
  userId?: string;
  counter?: number;

  constructor(username: string, phoneNumber: string, hour: string, date: string, explanation: string, randevuId?: string, userId?: string, counter?: number) {
    this.username = username;
    this.phoneNumber = phoneNumber;
    this.hour = hour;
    this.date = date;
    this.explanation = explanation;
    this.randevuId = randevuId;
    this.userId = userId;
    this.counter = counter;
  }
}