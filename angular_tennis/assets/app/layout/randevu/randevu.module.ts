import {NgModule} from "@angular/core";
import {RandevuRoutingModule} from "./randevu-routing.module";
import {ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RandevuAlComponent} from "./randevu-al/randevu-al.component";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {RandevuService} from "./randevu.service";

@NgModule({
  declarations: [
    RandevuAlComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RandevuRoutingModule,
    PageHeaderModule
  ],
  providers: [
      RandevuService
  ]
})
export class RandevuModule { }