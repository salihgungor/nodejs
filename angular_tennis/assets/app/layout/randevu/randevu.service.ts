import {Injectable} from "@angular/core";
import {Http, Response, Headers} from "@angular/http";
import 'rxjs/Rx';
import {Randevu} from "./randevu.model";
import {Observable} from "rxjs/Observable";

@Injectable()
export class RandevuService {

  private randevular: Randevu[] = [];
  constructor(private http: Http) { }

  addRandevu(randevu) {
    const body = JSON.stringify(randevu);
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    const token = localStorage.getItem('token')
        ? '?token='+localStorage.getItem('token')
        : '';
    return this.http.post('http://localhost:3000/randevu'+token, body, {headers: headers})
        .map((response: Response) => {
          const result = response.json();
          const randevu = new Randevu(
              result.obj.user.firstName,
              result.obj.user.phoneNumber,
              result.obj.hour,
              result.obj.date,
              result.obj.explanation,
              result.obj._id,
              result.obj.user._id);
          this.randevular.push(randevu);
          return randevu;
        })
        .catch((error: Response) => {
          /*this.errorService.handleError(error.json());*/
          return Observable.throw(error.json());
        });
  }

  addGuestRandevu(randevu) {
    const body = JSON.stringify(randevu);
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post('http://localhost:3000/randevu/guest-randevu', body, {headers: headers})
        .map((response: Response) => {
          const result = response.json();
          const randevu = new Randevu(
              result.obj.guestUsername,
              result.obj.guestPhoneNumber,
              result.obj.hour,
              result.obj.date,
              result.obj.explanation,
              '',
              '');
          this.randevular.push(randevu);
          return randevu;
        })
        .catch((error: Response) => {
          /*this.errorService.handleError(error.json());*/
          return Observable.throw(error.json());
        });
  }

  getAllRandevu() {
    return this.http.get('http://localhost:3000/randevu')
        .map((response: Response) => {
          const randevular = response.json().obj;
          let transformedRandevu: Randevu[] = [];
          let sayac: number = 0;
          for (let randevu of randevular) {
            if (randevu.hasOwnProperty('guestUsername')) {
              transformedRandevu.push(
                  new Randevu(
                      randevu.guestUsername+' (Misafir)',
                      randevu.guestPhoneNumber,
                      randevu.hour,
                      randevu.date,
                      randevu.explanation,
                      randevu._id,
                      '',
                      sayac
                  ));
              sayac++;
            } else {
              transformedRandevu.push(
                  new Randevu(
                      randevu.user.firstName,
                      randevu.user.phoneNumber,
                      randevu.hour,
                      randevu.date,
                      randevu.explanation,
                      randevu._id,
                      randevu.user._id,
                      sayac));
              sayac++;
            }
          }
          this.randevular = transformedRandevu;
          return transformedRandevu;
        })
        /*.catch((error: Response) => Observable.throw(error.json()));*/
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

}