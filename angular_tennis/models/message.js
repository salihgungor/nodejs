let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const User = require('./user');

let messageSchema = new Schema({
  content: { type: String, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User' }
});

messageSchema.post('remove', function (message) {
  User.findById(message.user, function (err, user) {
    user.messages.pull(message._id);
    user.save();
  });
});

module.exports = mongoose.model('Message', messageSchema);

/*

*   ilk parametre   modelin ismi
    ikinci parametre fieldların oluşturan model
    biz model ismine Message yazdık fakat otomatikmen o lowercase ve sonuna plural eki(s) gelecek.

* */