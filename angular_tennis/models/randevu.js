let mongoose = require('mongoose');
let Schema = mongoose.Schema;

const User = require('./user');

let randevuSchema = new Schema({
  hour: { type: String, required: true },
  date: { type: String, required: true },
  guestUsername: { type: String, required: false },
  guestPhoneNumber: { type: String, required: false },
  explanation : { type: String, required: false },
  user: { type: Schema.Types.ObjectId, ref: 'User' }
});

randevuSchema.post('remove', function (randevu) {
  User.findById(randevu.user, function (err, user) {
    user.randevus.pull(randevu._id);
    user.save();
  });
});

module.exports = mongoose.model('Randevu', randevuSchema);

/*

*   ilk parametre   modelin ismi
    ikinci parametre fieldların oluşturan model
    biz model ismine Message yazdık fakat otomatikmen o lowercase ve sonuna plural eki(s) gelecek.

* */
