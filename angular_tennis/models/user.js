let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let mongooseUniqueValidator = require('mongoose-unique-validator');

let userSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  phoneNumber: { type: String, required: true },
  randevu: [{ type: Schema.Types.ObjectId, ref: 'Randevu' }]           /* burada ilişkili bir yapı kurduk */
});

userSchema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('User', userSchema);    // employees olarak collection oluşacak.

/*
*   Javascriptte bir metoda parametre vereceksen bu parametre eğer bir metod ise yazarken parantezleri
*   yazmayacağız EVERY TİME. Onun dışında metodu normal kullanacağız.
*   ---------------------------------------------------------------------------------------------------
*   messages fieldı aslında bizim messages adındaki collectiona temsil edecek ve message eklendiği
*   anda oraya da eklenecek.
* */
