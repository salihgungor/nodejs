var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Randevu = require('../models/randevu');

router.get('/', function (req, res, next) {
  Randevu.find()
      .populate('user', 'firstName')
      .sort({date: 1})
      .exec(function (err, randevular) {
        if (err)
          return res.status(500).json({
            title: 'An error occurred.',
            error: err
          });
        res.status(200).json({
          message: 'Success',
          obj: randevular
        });
      });
});

/*router.use('/', function (req, res, next) {
  jwt.verify(req.query.token, 'secret', function (err, decoded) {
    if (err)
      return res.status(401).json({
        title: 'Not Authenticated',
        error: err
      });
    next();
  });
});*/

router.post('/', function (req, res, next) {
  let decoded = jwt.decode(req.query.token);
  User.findById(decoded.user._id, function (err, user) {
    if (err)
      return res.status(500).json({
        title: 'An error occurred.',
        error: err
      });
    let randevu = new Randevu({
      hour: req.body.hour,
      date: req.body.date,
      explanation: req.body.explanation,
      user: user._id,
    });
    randevu.save(function (err, result) {
      if (err)
        return res.status(500).json({
          title: 'An error occurred.',
          error: err
        });
      user.randevu.push(result);
      user.save();
      res.status(201).json({
        message: 'Saved message',
        obj: result
      });
    });
  });
});

router.post('/guest-randevu', function (req, res, next) {
  console.log('randevu: '+JSON.stringify(req.body));
  let randevu = new Randevu({
    hour: req.body.hour,
    date: req.body.date,
    guestUsername: req.body.username,
    guestPhoneNumber: req.body.phoneNumber,
    explanation: req.body.explanation
  });
  randevu.save(function (err, result) {
    if (err)
      return res.status(500).json({
        title: 'An error occurred.',
        error: err
      });
    console.log('result: '+JSON.stringify(result));
    res.status(201).json({
      message: 'Saved message',
      obj: result
    });
  });
});

router.patch('/:id', function (req, res, next) {
  let decoded = jwt.decode(req.query.token);
  Randevu.findById(req.params.id, function (err, randevu) {
    if (err)
      return res.status(500).json({
        title: 'An error occurred.',
        error: err
      });

    if (!randevu)
      return res.status(500).json({
        title: 'No message found.',
        error: {message: 'Message not found.'}
      });
    if (randevu.user != decoded.user._id) {
      return res.status(401).json({
        title: 'Not Authenticated',
        error: {message: 'Users do not match'}
      });
    }
    randevu.content = req.body.content;
    randevu.save(function (err, result) {
      if (err)
        return res.status(500).json({
          title: 'An error occurred.',
          error: err
        });
      res.status(200).json({
        message: 'Updated message',
        obj: result
      });
    });
  });
});

router.delete('/:id', function (req, res, next) {
  let decoded = jwt.decode(req.query.token);
  Randevu.findById(req.params.id, function (err, randevu) {
    if (err)
      return res.status(500).json({
        title: 'An error occurred.',
        error: err
      });

    if (!randevu)
      return res.status(500).json({
        title: 'No message found.',
        error: {message: 'Message not found.'}
      });
    if (randevu.user != decoded.user._id) {
      return res.status(401).json({
        title: 'Not Authenticated',
        error: {message: 'Users do not match'}
      });
    }
    randevu.remove(function (err, result) {
      if (err)
        return res.status(500).json({
          title: 'An error occurred.',
          error: err
        });
      res.status(200).json({
        message: 'Deleted message',
        obj: result
      });
    });
  });
});


module.exports = router;
