$(function () {

  const socket = io.connect('http://localhost:3000');

  $('#btn-chat').click(function (e) {
    e.preventDefault();
    let message = $('#btn-input').val();
    socket.emit('newMessage', message);
    $('#btn-input').val('');
  });
  let input = document.getElementById('btn-input');
  input.addEventListener('keyup', function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      document.getElementById('btn-chat').click();
    }
  });
  socket.on('sendMessage', function (message) {
    $('#chat').append(`<li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    ${message}
                                </p>
                            </div>
                        </li>`);
  });
});

