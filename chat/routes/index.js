var express = require('express');
var router = express.Router();
var test = require('../helpers/test');

/* GET home page. */
router.get('/', function(req, res, next) {


  res.render('index', {
    title: 'Express',
    username: req.cookies.username
  });
});



module.exports = router;


