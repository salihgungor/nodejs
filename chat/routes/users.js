var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const User = require('../model/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('register');
});

router.post('/', function(req, res, next) {
  console.log('user: '+JSON.stringify(req.body));
  let user = new User({
    username: req.body.user,
    password: req.body.pass
  });
  user.save();

  res.render('register');
});

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.post('/login', function(req, res, next) {
  User.find({username: req.body.user})
    .then(function (user) {
      if (user[0].password === req.body.pass) {
        jwt.sign({ user: user[0].username }, 'secretkey', { expiresIn: 1000*60*3 }, function (err, token) {
          res.cookie('jwt', token, { maxAge: 1000*60*3, httpOnly: true });
          res.cookie('username', user[0].username, { maxAge: 1000*60*3, httpOnly: true }); // 3dk
          res.redirect('/');
        });
      } else {
        console.log('parolayı yanlış girdiniz.');
        res.end();
      }
    })
    .catch(function (err) {
      throw err;
    });

});

router.get('/secure', verifyToken, function(req, res, next) {
  console.log('req.token: '+JSON.stringify(req.token));
  jwt.verify(req.token, 'secretkey', function (err, authData) {
    if (err) throw err;
    res.json({
      text: 'güvenli',
      data: authData
    });
  });
});

function verifyToken(req, res, next) {
  const token = req.cookies.jwt;
  if (token){
    req.token = token;
    next();
  } else {
    console.log('yetkiniz yoktur.');
  }

/*  console.log('headers: '+JSON.stringify(req.cookies.token));
  const bearerHeader = req.headers['authorization'];
  console.log('ber: '+bearerHeader);
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    console.log('yetkiniz yoktur.');
  }*/

}

module.exports = router;
