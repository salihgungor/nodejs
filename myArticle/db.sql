--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: add_article(character varying, text, date, integer, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_article(baslik character varying, icerik text, tarih date, dilid integer, kategoriid integer, silindi_mi boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
      INSERT INTO makale(baslik, icerik, tarih, dilid, kategoriid, silindi_mi) VALUES (baslik, icerik, tarih, dilid, kategoriid, silindi_mi);
    END;
    $$;


ALTER FUNCTION public.add_article(baslik character varying, icerik text, tarih date, dilid integer, kategoriid integer, silindi_mi boolean) OWNER TO postgres;

--
-- Name: add_comment(text, date, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_comment(icerik text, tarih date, makaleid integer, kullaniciid integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
      INSERT INTO yorum(icerik, tarih, makaleid, kullaniciid) VALUES (icerik, tarih, makaleid, kullaniciid);
    END;
    $$;


ALTER FUNCTION public.add_comment(icerik text, tarih date, makaleid integer, kullaniciid integer) OWNER TO postgres;

--
-- Name: add_user(character varying, character varying, character varying, character varying, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_user(adi character varying, soyadi character varying, username character varying, parola character varying, rolid integer, meslekid integer, cinsiyetid integer, uyrukid integer, ilid integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
      INSERT INTO kullanici(adi, soyadi, username, parola, rolid,meslekid,cinsiyetid,uyrukid,ilid) VALUES (adi, soyadi, username, parola, rolid, meslekid,cinsiyetid, uyrukid, ilid);
    END;
    $$;


ALTER FUNCTION public.add_user(adi character varying, soyadi character varying, username character varying, parola character varying, rolid integer, meslekid integer, cinsiyetid integer, uyrukid integer, ilid integer) OWNER TO postgres;

--
-- Name: fonksiyontanimlama(text, smallint, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION fonksiyontanimlama(mesaj text, altkaraktersayisi smallint, tekrarsayisi integer) RETURNS text
    LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
    AS $$
DECLARE
    sonuc TEXT; -- Değişken tanımlama bloğu
BEGIN
    sonuc := '';
    IF tekrarSayisi > 0 THEN
        FOR i IN 1 .. tekrarSayisi LOOP
            sonuc := sonuc || i || '.' || SUBSTRING(mesaj FROM 1 FOR altKarakterSayisi) || E'\r\n';
            -- E: string içerisindeki (E)scape karakterleri için...
        END LOOP;
    END IF;
    RETURN sonuc;
END;
$$;


ALTER FUNCTION public.fonksiyontanimlama(mesaj text, altkaraktersayisi smallint, tekrarsayisi integer) OWNER TO postgres;

--
-- Name: kayitdolanimi(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION kayitdolanimi() RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    musteriler kullanici%ROWTYPE; -- customer."CustomerID"%TYPE
    sonuc TEXT;
BEGIN
    sonuc := '';
    FOR musteriler IN SELECT * FROM kullanici LOOP
        sonuc := sonuc || musteriler."kullaniciid" || E'\t' || musteriler."adi" || E'\r\n';
    END LOOP;
    RETURN sonuc;
END;
$$;


ALTER FUNCTION public.kayitdolanimi() OWNER TO postgres;

--
-- Name: personelara(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION personelara(personelno integer) RETURNS TABLE(numara integer, adi character varying, soyadi character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY SELECT "public"."kullaniciid", "adi", "soyadi" FROM kullanici WHERE "kullaniciid" = personelNo;
END;
$$;


ALTER FUNCTION public.personelara(personelno integer) OWNER TO postgres;

--
-- Name: personelaraa(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION personelaraa(personelno integer) RETURNS TABLE(numara integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY SELECT "kullaniciid" FROM kullanici WHERE "kullaniciid" = personelNo;
END;
$$;


ALTER FUNCTION public.personelaraa(personelno integer) OWNER TO postgres;

--
-- Name: personelaraaa(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION personelaraaa(personelno integer) RETURNS TABLE(numara integer, adi character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY SELECT "kullaniciid", "adi" FROM kullanici WHERE "kullaniciid" = personelNo;
END;
$$;


ALTER FUNCTION public.personelaraaa(personelno integer) OWNER TO postgres;

--
-- Name: personelaraaaa(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION personelaraaaa(personelno integer) RETURNS TABLE(numara integer, addi character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY SELECT "kullaniciid", "adi" FROM kullanici WHERE "kullaniciid" = personelNo;
END;
$$;


ALTER FUNCTION public.personelaraaaa(personelno integer) OWNER TO postgres;

--
-- Name: show_roll(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION show_roll() RETURNS TABLE(rolid integer, adi character varying)
    LANGUAGE plpgsql
    AS $$
    BEGIN 
    RETURN QUERY SELECT * FROM rol; 
    END; 
    $$;


ALTER FUNCTION public.show_roll() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: durum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE durum (
    durumid integer NOT NULL,
    durum boolean NOT NULL
);


ALTER TABLE durum OWNER TO postgres;

--
-- Name: Durum_durumid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Durum_durumid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Durum_durumid_seq" OWNER TO postgres;

--
-- Name: Durum_durumid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Durum_durumid_seq" OWNED BY durum.durumid;


--
-- Name: kullanici; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE kullanici (
    kullaniciid integer NOT NULL,
    adi character varying(2044) NOT NULL,
    soyadi character varying(2044) NOT NULL,
    parola character varying(2044) NOT NULL,
    username character varying(2044) NOT NULL,
    rolid integer NOT NULL,
    cinsiyetid integer,
    ilid integer,
    uyrukid integer,
    durumid integer,
    meslekid integer
);


ALTER TABLE kullanici OWNER TO postgres;

--
-- Name: Kullanici_KullaniciID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Kullanici_KullaniciID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Kullanici_KullaniciID_seq" OWNER TO postgres;

--
-- Name: Kullanici_KullaniciID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Kullanici_KullaniciID_seq" OWNED BY kullanici.kullaniciid;


--
-- Name: cinsiyet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cinsiyet (
    cinsiyetid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE cinsiyet OWNER TO postgres;

--
-- Name: cinsiyet_adi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cinsiyet_adi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cinsiyet_adi_seq OWNER TO postgres;

--
-- Name: cinsiyet_adi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cinsiyet_adi_seq OWNED BY cinsiyet.adi;


--
-- Name: cinsiyet_cinsiyetid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cinsiyet_cinsiyetid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cinsiyet_cinsiyetid_seq OWNER TO postgres;

--
-- Name: cinsiyet_cinsiyetid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cinsiyet_cinsiyetid_seq OWNED BY cinsiyet.cinsiyetid;


--
-- Name: etiket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE etiket (
    etiketid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE etiket OWNER TO postgres;

--
-- Name: etiket_etiketid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE etiket_etiketid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE etiket_etiketid_seq OWNER TO postgres;

--
-- Name: etiket_etiketid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE etiket_etiketid_seq OWNED BY etiket.etiketid;


--
-- Name: il; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE il (
    ilid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE il OWNER TO postgres;

--
-- Name: il_ilid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE il_ilid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE il_ilid_seq OWNER TO postgres;

--
-- Name: il_ilid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE il_ilid_seq OWNED BY il.ilid;


--
-- Name: kategori; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE kategori (
    kategoriid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE kategori OWNER TO postgres;

--
-- Name: kategori_kategoriid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kategori_kategoriid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kategori_kategoriid_seq OWNER TO postgres;

--
-- Name: kategori_kategoriid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kategori_kategoriid_seq OWNED BY kategori.kategoriid;


--
-- Name: log_makale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE log_makale (
    log_makale_id character varying(2044) NOT NULL,
    log character varying(2044) NOT NULL,
    tarih date
);


ALTER TABLE log_makale OWNER TO postgres;

--
-- Name: log_makale_log_makale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_makale_log_makale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_makale_log_makale_id_seq OWNER TO postgres;

--
-- Name: log_makale_log_makale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_makale_log_makale_id_seq OWNED BY log_makale.log_makale_id;


--
-- Name: makale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE makale (
    makaleid integer NOT NULL,
    baslik character varying(2044) NOT NULL,
    icerik text NOT NULL,
    dilid integer NOT NULL,
    tarih date,
    kategoriid integer,
    silindi_mi boolean
);


ALTER TABLE makale OWNER TO postgres;

--
-- Name: makale_etiket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE makale_etiket (
    makale_etiket_id integer NOT NULL,
    makaleid integer NOT NULL,
    etiketid integer NOT NULL
);


ALTER TABLE makale_etiket OWNER TO postgres;

--
-- Name: makale_etiket_makale_etiket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE makale_etiket_makale_etiket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE makale_etiket_makale_etiket_id_seq OWNER TO postgres;

--
-- Name: makale_etiket_makale_etiket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE makale_etiket_makale_etiket_id_seq OWNED BY makale_etiket.makale_etiket_id;


--
-- Name: meslek; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE meslek (
    meslekid integer NOT NULL,
    meslek character varying(2044) NOT NULL
);


ALTER TABLE meslek OWNER TO postgres;

--
-- Name: meslek_meslekid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE meslek_meslekid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meslek_meslekid_seq OWNER TO postgres;

--
-- Name: meslek_meslekid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE meslek_meslekid_seq OWNED BY meslek.meslekid;


--
-- Name: programlamadili; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE programlamadili (
    dilid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE programlamadili OWNER TO postgres;

--
-- Name: programlamadili_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE programlamadili_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE programlamadili_id_seq OWNER TO postgres;

--
-- Name: programlamadili_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE programlamadili_id_seq OWNED BY programlamadili.dilid;


--
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE rol (
    "rolId" integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE rol OWNER TO postgres;

--
-- Name: rol_rolId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "rol_rolId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "rol_rolId_seq" OWNER TO postgres;

--
-- Name: rol_rolId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "rol_rolId_seq" OWNED BY rol."rolId";


--
-- Name: soru_SoruID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "soru_SoruID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "soru_SoruID_seq" OWNER TO postgres;

--
-- Name: soru_SoruID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "soru_SoruID_seq" OWNED BY makale.makaleid;


--
-- Name: uyruk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE uyruk (
    uyrukid integer NOT NULL,
    adi character varying(2044) NOT NULL
);


ALTER TABLE uyruk OWNER TO postgres;

--
-- Name: uyruk_uyrukid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE uyruk_uyrukid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uyruk_uyrukid_seq OWNER TO postgres;

--
-- Name: uyruk_uyrukid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE uyruk_uyrukid_seq OWNED BY uyruk.uyrukid;


--
-- Name: yorum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE yorum (
    yorumid integer NOT NULL,
    icerik text NOT NULL,
    tarih date,
    kullaniciid integer NOT NULL,
    makaleid integer NOT NULL
);


ALTER TABLE yorum OWNER TO postgres;

--
-- Name: yorum_yorumid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE yorum_yorumid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE yorum_yorumid_seq OWNER TO postgres;

--
-- Name: yorum_yorumid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE yorum_yorumid_seq OWNED BY yorum.yorumid;


--
-- Name: cinsiyet cinsiyetid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cinsiyet ALTER COLUMN cinsiyetid SET DEFAULT nextval('cinsiyet_cinsiyetid_seq'::regclass);


--
-- Name: cinsiyet adi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cinsiyet ALTER COLUMN adi SET DEFAULT nextval('cinsiyet_adi_seq'::regclass);


--
-- Name: durum durumid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY durum ALTER COLUMN durumid SET DEFAULT nextval('"Durum_durumid_seq"'::regclass);


--
-- Name: etiket etiketid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etiket ALTER COLUMN etiketid SET DEFAULT nextval('etiket_etiketid_seq'::regclass);


--
-- Name: il ilid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY il ALTER COLUMN ilid SET DEFAULT nextval('il_ilid_seq'::regclass);


--
-- Name: kategori kategoriid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kategori ALTER COLUMN kategoriid SET DEFAULT nextval('kategori_kategoriid_seq'::regclass);


--
-- Name: kullanici kullaniciid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici ALTER COLUMN kullaniciid SET DEFAULT nextval('"Kullanici_KullaniciID_seq"'::regclass);


--
-- Name: log_makale log_makale_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_makale ALTER COLUMN log_makale_id SET DEFAULT nextval('log_makale_log_makale_id_seq'::regclass);


--
-- Name: makale makaleid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale ALTER COLUMN makaleid SET DEFAULT nextval('"soru_SoruID_seq"'::regclass);


--
-- Name: makale_etiket makale_etiket_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale_etiket ALTER COLUMN makale_etiket_id SET DEFAULT nextval('makale_etiket_makale_etiket_id_seq'::regclass);


--
-- Name: meslek meslekid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY meslek ALTER COLUMN meslekid SET DEFAULT nextval('meslek_meslekid_seq'::regclass);


--
-- Name: programlamadili dilid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programlamadili ALTER COLUMN dilid SET DEFAULT nextval('programlamadili_id_seq'::regclass);


--
-- Name: rol rolId; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rol ALTER COLUMN "rolId" SET DEFAULT nextval('"rol_rolId_seq"'::regclass);


--
-- Name: uyruk uyrukid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyruk ALTER COLUMN uyrukid SET DEFAULT nextval('uyruk_uyrukid_seq'::regclass);


--
-- Name: yorum yorumid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yorum ALTER COLUMN yorumid SET DEFAULT nextval('yorum_yorumid_seq'::regclass);


--
-- Name: Durum_durumid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Durum_durumid_seq"', 2, true);


--
-- Name: Kullanici_KullaniciID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Kullanici_KullaniciID_seq"', 20, true);


--
-- Data for Name: cinsiyet; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cinsiyet VALUES (1, 'bay');
INSERT INTO cinsiyet VALUES (2, 'bayan');


--
-- Name: cinsiyet_adi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cinsiyet_adi_seq', 1, true);


--
-- Name: cinsiyet_cinsiyetid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cinsiyet_cinsiyetid_seq', 3, true);


--
-- Data for Name: durum; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO durum VALUES (1, true);
INSERT INTO durum VALUES (2, false);


--
-- Data for Name: etiket; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: etiket_etiketid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('etiket_etiketid_seq', 1, false);


--
-- Data for Name: il; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO il VALUES (1, 'İstanbul');
INSERT INTO il VALUES (2, 'Ankara');
INSERT INTO il VALUES (3, 'Sakarya');
INSERT INTO il VALUES (4, 'Kocaeli');
INSERT INTO il VALUES (5, 'Düzce');
INSERT INTO il VALUES (6, 'Bitlis');
INSERT INTO il VALUES (7, 'Van');
INSERT INTO il VALUES (8, 'Mardin');
INSERT INTO il VALUES (9, 'Ağrı');
INSERT INTO il VALUES (10, 'Şanlı Urfa');
INSERT INTO il VALUES (11, 'Mersin');
INSERT INTO il VALUES (12, 'Tekirdağ');


--
-- Name: il_ilid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('il_ilid_seq', 12, true);


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO kategori VALUES (1, 'Yazılım');
INSERT INTO kategori VALUES (2, 'Network');
INSERT INTO kategori VALUES (3, 'Tasarım');
INSERT INTO kategori VALUES (4, 'Sanal gerçeklik');
INSERT INTO kategori VALUES (5, 'IoT');
INSERT INTO kategori VALUES (6, 'Gömülü Sistemler');


--
-- Name: kategori_kategoriid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kategori_kategoriid_seq', 7, true);


--
-- Data for Name: kullanici; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO kullanici VALUES (16, 'salih', 'gungor', '123', 'salihgungor', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO kullanici VALUES (17, 'salih', 'gungor', '$2a$10$t6zwZAoR5MS5Aix0M8Xj.uzR7Zs4bRaDzT3U1gTPlu9853rKD7NOW', 'salih', 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO kullanici VALUES (18, 'procedure', 'procedure', '123', 'procedure', 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO kullanici VALUES (19, 'genel', 'test', '123', 'genel test', 2, 1, 1, 1, NULL, 1);
INSERT INTO kullanici VALUES (20, 'salih', 'gybgor', '123123123', 'asd', 1, 1, 1, 1, 1, 1);


--
-- Data for Name: log_makale; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO log_makale VALUES ('1', 'Guncellendi', NULL);
INSERT INTO log_makale VALUES ('2', 'Guncellendi test', '2017-10-10');
INSERT INTO log_makale VALUES ('3', 'Guncellendi test', '2017-10-10');
INSERT INTO log_makale VALUES ('11', '', NULL);


--
-- Name: log_makale_log_makale_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_makale_log_makale_id_seq', 1, true);


--
-- Data for Name: makale; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO makale VALUES (13, 'makale yazdım', 'makale yazıdım bakalım olacak mı', 1, '2017-12-13', 1, false);
INSERT INTO makale VALUES (14, 'tarihli makale tarihli makale', 'tarihli makaletarihli makaletarihli makaletarihli makaletarihli makaletarihli makaletarihli makaletarihli makaletarihli makaletarihli makale', 1, '2017-12-13', 1, false);
INSERT INTO makale VALUES (15, 'abdulvahap için örnek', 'abdulvahap için örnekabdulvahap için örnekabdulvahap için örnekabdulvahap için örnekabdulvahap için örnekabdulvahap için örnekabdulvahap içi', 1, '2017-12-13', 1, false);
INSERT INTO makale VALUES (5, 'tarihli makale', 'test amaçlı tarihli makale ekledik', 3, '2017-12-09', 2, false);
INSERT INTO makale VALUES (7, 'makale', 'mkalaesdasdsa
d
sa
d
sad
sa
d
as
d
as
d
sadas
d
sa
das
d
sa
d
sad
sa
d
s
d', 3, '2017-12-09', 5, false);
INSERT INTO makale VALUES (9, 'asdsadasdsadsd', 'asdsadsadsadsadsadsad
as
d
asdsa
d
sadasdsadsdsad', 1, '2017-12-11', 1, false);
INSERT INTO makale VALUES (10, 'store', 'procedure', 1, '2017-10-10', 2, false);
INSERT INTO makale VALUES (11, 'store procedure ile yaptığım makale eklemesini yapıyorum', 'store procedure ile yaptığım makale eklemesini yapıyorum
bakalım olacak mı', 3, '2017-12-11', 2, false);
INSERT INTO makale VALUES (8, 'yeni', 'şu an en yeni makale bu bundan sonra eklenmezse tabi.', 4, '2017-12-10', 6, true);
INSERT INTO makale VALUES (2, 'test', 'asdfasfasf

asf

asf
as
fasfasfasfasfasfsf', 1, '2017-12-09', 1, true);
INSERT INTO makale VALUES (6, 'test', 'dasd
sa
da
sd
as
d
as
d
asdasdasd', 1, '2017-12-09', 3, true);


--
-- Data for Name: makale_etiket; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: makale_etiket_makale_etiket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('makale_etiket_makale_etiket_id_seq', 1, false);


--
-- Data for Name: meslek; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO meslek VALUES (1, 'Bilgisayar Mühendisi');
INSERT INTO meslek VALUES (2, 'Yazılım Mühendisi');
INSERT INTO meslek VALUES (3, 'Elektirik-Elektronik Mühendisi');
INSERT INTO meslek VALUES (4, 'Bilişim Sistemleri MÜhendisi');
INSERT INTO meslek VALUES (5, 'Makine Mühendisi');
INSERT INTO meslek VALUES (6, 'Mekatronik Mühendisi');
INSERT INTO meslek VALUES (7, 'Biyomedikal Mühendisi');


--
-- Name: meslek_meslekid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('meslek_meslekid_seq', 7, true);


--
-- Data for Name: programlamadili; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO programlamadili VALUES (1, 'Javascript');
INSERT INTO programlamadili VALUES (2, 'Java');
INSERT INTO programlamadili VALUES (3, 'C++');
INSERT INTO programlamadili VALUES (4, 'C#');


--
-- Name: programlamadili_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('programlamadili_id_seq', 4, true);


--
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rol VALUES (1, 'admin');
INSERT INTO rol VALUES (2, 'user');


--
-- Name: rol_rolId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"rol_rolId_seq"', 2, true);


--
-- Name: soru_SoruID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"soru_SoruID_seq"', 15, true);


--
-- Data for Name: uyruk; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO uyruk VALUES (1, 'Türkiye');
INSERT INTO uyruk VALUES (2, 'Almanya');
INSERT INTO uyruk VALUES (3, 'ABD');
INSERT INTO uyruk VALUES (4, 'Rusya');
INSERT INTO uyruk VALUES (5, 'Malezye');
INSERT INTO uyruk VALUES (6, 'Şili');
INSERT INTO uyruk VALUES (7, 'Brezilya');
INSERT INTO uyruk VALUES (8, 'Arjantin');
INSERT INTO uyruk VALUES (9, 'Azerbaycan');
INSERT INTO uyruk VALUES (10, 'Kıbrıs');
INSERT INTO uyruk VALUES (11, 'Norveç');
INSERT INTO uyruk VALUES (12, 'Hollanda');
INSERT INTO uyruk VALUES (13, 'Fransa');


--
-- Name: uyruk_uyrukid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('uyruk_uyrukid_seq', 13, true);


--
-- Data for Name: yorum; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO yorum VALUES (1, 'bu makaleyi çok beğendim', '2017-11-12', 16, 9);
INSERT INTO yorum VALUES (2, 'çok güzel', '2017-10-12', 16, 9);
INSERT INTO yorum VALUES (3, 'bu benim ilk yorumum', '2017-12-11', 16, 9);
INSERT INTO yorum VALUES (4, 'store denemeeeeee
store denemeeeeee
store denemeeeeee', '2017-12-11', 16, 11);


--
-- Name: yorum_yorumid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('yorum_yorumid_seq', 4, true);


--
-- Name: durum Durum_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY durum
    ADD CONSTRAINT "Durum_pkey" PRIMARY KEY (durumid);


--
-- Name: cinsiyet cinsiyet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cinsiyet
    ADD CONSTRAINT cinsiyet_pkey PRIMARY KEY (cinsiyetid);


--
-- Name: etiket etiket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY etiket
    ADD CONSTRAINT etiket_pkey PRIMARY KEY (etiketid);


--
-- Name: il il_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY il
    ADD CONSTRAINT il_pkey PRIMARY KEY (ilid);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (kategoriid);


--
-- Name: log_makale log_makale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_makale
    ADD CONSTRAINT log_makale_pkey PRIMARY KEY (log_makale_id);


--
-- Name: makale_etiket makale_etiket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale_etiket
    ADD CONSTRAINT makale_etiket_pkey PRIMARY KEY (makale_etiket_id);


--
-- Name: meslek meslek_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY meslek
    ADD CONSTRAINT meslek_pkey PRIMARY KEY (meslekid);


--
-- Name: programlamadili programlamadili_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programlamadili
    ADD CONSTRAINT programlamadili_pkey PRIMARY KEY (dilid);


--
-- Name: rol rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY ("rolId");


--
-- Name: makale soru_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale
    ADD CONSTRAINT soru_pkey PRIMARY KEY (makaleid);


--
-- Name: kullanici unique_KullaniciID; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT "unique_KullaniciID" PRIMARY KEY (kullaniciid);


--
-- Name: yorum unique_yorumid; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yorum
    ADD CONSTRAINT unique_yorumid UNIQUE (yorumid);


--
-- Name: uyruk uyruk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uyruk
    ADD CONSTRAINT uyruk_pkey PRIMARY KEY (uyrukid);


--
-- Name: yorum yorum_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yorum
    ADD CONSTRAINT yorum_pkey PRIMARY KEY (yorumid);


--
-- Name: index_log_makale_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_log_makale_id ON log_makale USING btree (log_makale_id);


--
-- Name: kullanici lnk_cinsiyet_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_cinsiyet_kullanici FOREIGN KEY (cinsiyetid) REFERENCES cinsiyet(cinsiyetid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kullanici lnk_durum_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_durum_kullanici FOREIGN KEY (durumid) REFERENCES durum(durumid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: makale_etiket lnk_etiket_makale_etiket; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale_etiket
    ADD CONSTRAINT lnk_etiket_makale_etiket FOREIGN KEY (etiketid) REFERENCES etiket(etiketid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kullanici lnk_il_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_il_kullanici FOREIGN KEY (ilid) REFERENCES il(ilid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: makale lnk_kategori_makale; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale
    ADD CONSTRAINT lnk_kategori_makale FOREIGN KEY (kategoriid) REFERENCES kategori(kategoriid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: yorum lnk_kullanici_yorum; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yorum
    ADD CONSTRAINT lnk_kullanici_yorum FOREIGN KEY (kullaniciid) REFERENCES kullanici(kullaniciid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: makale_etiket lnk_makale_makale_etiket; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale_etiket
    ADD CONSTRAINT lnk_makale_makale_etiket FOREIGN KEY (makaleid) REFERENCES makale(makaleid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: yorum lnk_makale_yorum; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY yorum
    ADD CONSTRAINT lnk_makale_yorum FOREIGN KEY (makaleid) REFERENCES makale(makaleid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kullanici lnk_meslek_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_meslek_kullanici FOREIGN KEY (meslekid) REFERENCES meslek(meslekid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: makale lnk_programlamadili_soru; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY makale
    ADD CONSTRAINT lnk_programlamadili_soru FOREIGN KEY (dilid) REFERENCES programlamadili(dilid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kullanici lnk_rol_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_rol_kullanici FOREIGN KEY (rolid) REFERENCES rol("rolId") MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kullanici lnk_uyruk_kullanici; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kullanici
    ADD CONSTRAINT lnk_uyruk_kullanici FOREIGN KEY (uyrukid) REFERENCES uyruk(uyrukid) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

