var express = require('express');
var router = express.Router();
const pg = require('pg');

let datetime = new Date();

const config = {
    user: 'postgres',
    host: 'localhost',
    database: 'forum',
    password: '123',
    port: 5432
  };
  
const pool = new pg.Pool(config);
pool.connect(function(err, client, done){
    if(err)
        console.log('DB ye bağlanti sağlanamadı.'+err); 
});

router.get('/', function(req, res, next) {
    getAllArticle()
    .then(function(makaleler){
        getAllCategory()
        .then(function(kategoriler){
            getFreshArticle()
            .then(function(freshArticle){
                let result = {
                    kategoriler: kategoriler,
                    makaleler: makaleler,
                    freshes: freshArticle
                };
                res.render('index', result); 
            });            
        });
    });
});

router.get('/makale/:id', function(req, res, next) {
    getListArticle(req.params.id)
    .then(function(makaleler){
        getAllCategory()
        .then(function(kategoriler){
            getFreshArticle()
            .then(function(freshArticle){
                let result = {
                    kategoriler: kategoriler,
                    makaleler: makaleler,
                    freshes: freshArticle
                };
                res.render('index', result); 
            });              
        });
    });
});

router.get('/fresh/:id', function(req, res, next) {
    getFreshListArticle(req.params.id)
    .then(function(makaleler){
        getAllCategory()
        .then(function(kategoriler){
            getFreshArticle()
            .then(function(freshArticle){
                let result = {
                    kategoriler: kategoriler,
                    makaleler: makaleler,
                    freshes: freshArticle
                };
                res.render('index', result); 
            });              
        });
    });
});

router.get('/makaleOku/:id', function(req, res, next) {
    getArticle(req.params.id)
    .then(function(makale){
        getCommentByArticle(req.params.id)
        .then(function(yorumlar){
            let result = {
                makale: makale,
                yorumlar: yorumlar
            };
            res.render('makaleOku', result); 
        });
    });
});

router.get('/makaleYaz', ensureAuthenticated, function(req, res, next) {
    getAllLangues()
    .then(function(diller){
        getAllCategory()
        .then(function(kategoriler){
            let result = {
                kategoriler: kategoriler,
                diller: diller
            };
            res.render('makaleYaz', result); 
        });
    });
});

router.post('/makaleYaz', function(req, res, next) {
    let data = {
        Baslik: req.body.baslik,
        Icerik: req.body.icerik,
        Dil: req.body.dil,
        EklenmeTarihi: datetime,
        Kategori: req.body.kategori,
        Silindi_mi: false
    };
        let query = 'select add_article($1, $2, $3, $4, $5, $6)';
        pool.query(query, [data.Baslik, data.Icerik, data.EklenmeTarihi, data.Dil, data.Kategori, data.Silindi_mi], function(err, result){
            if(err) 
                console.log('Sorguda bir problem var'+err);
        });
    res.redirect('/');
});

router.post('/yorumYap', function(req, res, next) {
    let data = {
        Baslik: req.body.baslik,
        Icerik: req.body.icerik,
        Tarih: datetime,
        Makale: req.body.makaleid,
        Kullanici: req.body.kullaniciid
    };
    data.Kullanici = 16;
        let query = 'select add_comment($1, $2, $3, $4)';
        pool.query(query, [data.Icerik, data.Tarih, data.Makale, data.Kullanici], function(err, result){
            if(err) 
                console.log('Sorguda bir problem var'+err);
        });
    res.redirect('/');
});

router.post('/makaleSil', function(req, res, next) {
    let data = {
        makaleid: req.body.makaleid,
    };
        let query = 'select delete_article($1)';
        pool.query(query, [data.makaleid], function(err, result){
            if(err) 
                console.log('Sorguda bir problem var'+err);
        });
    res.redirect('/');
});

function getAllLangues(){
    return new Promise(function(resolve, reject){
        let query = 'select * from programlamadili';
        pool.query(query, function(err, language, fields){
            if(err) 
                console.log('error: '+err);
            resolve(language.rows);
        });
    }); 
}
function getAllArticle(){
    return new Promise(function(resolve, reject){
        let query = 'select makaleid,baslik,icerik,tarih,silindi_mi,kategori.adi FROM makale inner join kategori on kategori.kategoriid = makale.kategoriid where silindi_mi = false';
        pool.query(query, function(err, article, fields){
            if(err) 
                console.log('error: '+err);
            resolve(article.rows);
        });
    }); 
}
function getListArticle(id){
    return new Promise(function(resolve, reject){
        let query = 'select makale.makaleid,makale.baslik,makale.icerik,makale.tarih,kategori.adi FROM makale inner join kategori on makale.kategoriid = kategori.kategoriid where silindi_mi = false and kategori.kategoriid = '+id;
        pool.query(query, function(err, article, fields){
            if(err) 
                console.log('error: '+err);
            resolve(article.rows);
        });
    }); 
}
function getFreshListArticle(id){
    return new Promise(function(resolve, reject){
        let query = 'select makale.makaleid,makale.baslik,makale.icerik,makale.tarih,kategori.adi FROM makale inner join kategori on makale.kategoriid = kategori.kategoriid where silindi_mi = false and makale.makaleid = '+id;
        pool.query(query, function(err, article, fields){
            if(err) 
                console.log('error: '+err);
            resolve(article.rows);
        });
    }); 
}
function getAllCategory(){
    return new Promise(function(resolve, reject){
        let query = 'select * from kategori';
        pool.query(query, function(err, category, fields){
            if(err) 
                console.log('error: '+err);
            resolve(category.rows);
        });
    }); 
}
function getCommentByArticle(id){
    return new Promise(function(resolve, reject){
        let query = 'select * from yorum where makaleid = '+id;
        pool.query(query, function(err, comment, fields){
            if(err) 
                console.log('error: '+err);
            resolve(comment.rows);
        });
    }); 
}
function getFreshArticle(){
    return new Promise(function(resolve, reject){
        let query = 'select makaleid, baslik from makale where silindi_mi = false order by tarih desc limit 5 offset 0';
        pool.query(query, function(err, article, fields){
            if(err) 
                console.log('error: '+err);
            resolve(article.rows);
        });
    }); 
}
function getArticle(id){
    return new Promise(function(resolve, reject){
        let query = 'select * from makale where makaleid = '+id;
        pool.query(query, function(err, article, fields){
            console.log('makale: '+article.rows);
            if(err) 
                console.log('error: '+err);
            resolve(article.rows);
        });
    }); 
}
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','Buraya giriş yetkiniz yoktur.');
		res.redirect('/users/login');
	}
}
module.exports = router;
