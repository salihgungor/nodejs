var express = require('express');
var router = express.Router();
const pg = require('pg');
let bcrypt = require('bcryptjs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

const config = {
    user: 'postgres',
    host: 'localhost',
    database: 'forum',
    password: '123',
    port: 5432
  };
  
const pool = new pg.Pool(config);
pool.connect(function(err, client, done){
    if(err)
        console.log('DB ye bağlanti sağlanamadı.'+err); 
});

router.get('/login', function(req, res, next) {
    res.render('login');
});

router.post('/login',
passport.authenticate('local', {successRedirect:'/', failureRedirect:'/users/login',failureFlash: true}),
function(req, res) {
    res.redirect('/');
});

router.get('/register', function(req, res, next) {
    getAllRols()
    .then(function(rols){
        getAllCinsiyet()
        .then(function(cinsiyetler){
            getAllCity()
            .then(function(sehirler){
                getAllCounteres()
                .then(function(uyruklar){
                    getAllMeslek()
                    .then(function(meslekler){
                        let user = {
                            rols: rols,
                            cinsiyetler: cinsiyetler,
                            sehirler, sehirler,
                            uyruklar: uyruklar,
                            meslekler: meslekler
                        }
                        res.render('register', user);                         
                    });
                });
            });
        });
    });
});

router.post('/register', function(req, res, next) {
    let newUser = {
        Adi: req.body.name,
        Soyadi: req.body.surName,
        UserName: req.body.userName,
        Parola: req.body.password,
        Rol: req.body.rol,
        Meslek: req.body.meslek,
        Cinsiyet: req.body.cinsiyet,
        Uyruk: req.body.uyruk,
        Sehir: req.body.sehir
    };
    createUser(newUser)
    .then(function(result){
        req.flash('success_msg', 'Başarıyla kayıt oldunuz.');
        res.redirect('/users/login');
    });

});

router.get('/logout', function(req, res){
    req.logout();
    req.flash('success_msg', 'Başarıyla çıkış yaptınız');
    res.redirect('/');
});

passport.use(new LocalStrategy(function(username, password, done) {
    getUserByUsername(16)
    .then(function(user){
        if(!user)
            return done(null, false, {message: 'Bilinmeyen Kullanıcı'});
        comparePassword(password, user.parola)
        .then(function(isMatch){
            if(isMatch){
                return done(null, user); 
            } else {
                return done(null, false, {message: 'Parola hatalı'});
            }
        });
    });
}));
passport.serializeUser(function(user, done) {
    done(null, user.kullaniciid);
});
  
passport.deserializeUser(function(id, done) {
    getUserById(id)
    .then(function(user){
        done(null, user);
    });
});
function createUser(newUser){
    return new Promise(function(resolve, reject){
                let query = 'select add_user($1, $2, $3, $4, $5, $6, $7, $8, $9)';
                pool.query(query, [newUser.Adi, newUser.Soyadi, newUser.UserName, newUser.Parola, newUser.Rol, newUser.Meslek, newUser.Cinsiyet, newUser.Uyruk, newUser.Sehir], function(err, result){
                    if(err) 
                        reject(err);
                    resolve(result);
                });   
    });
}
/* function createUser(newUser){
    return new Promise(function(resolve, reject){
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(newUser.Parola, salt, function(err, hash) {
                newUser.Parola = hash;
                let query = 'insert into kullanici (adi, soyadi, username, parola, rolid) values($1, $2, $3, $4, $5)';
                pool.query(query, [newUser.Adi, newUser.Soyadi, newUser.UserName, newUser.Parola, newUser.Rol], function(err, result){
                    if(err) 
                        reject(err);
                    resolve(result);
                });
            });
        });    
    });
} */
function getUserByUsername(username){
    return new Promise(function(resolve, reject){
        let query = 'select * from kullanici where kullaniciid = '+username;
        pool.query(query, function(err, user){
            if(err) 
                reject(err);
            resolve(user.rows[0]);
        });
    });
}
function comparePassword(candidatePassword, hash){
    console.log('candi: '+candidatePassword + ' pass: '+hash);
    return new Promise(function(resolve, reject){
       if(candidatePassword == hash)
            resolve(true);
        resolve(false);
    });
}
/* function comparePassword(candidatePassword, hash){
    return new Promise(function(resolve, reject){
        bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
            if(err) 
                reject(err);
            resolve(null, isMatch);
        });
    });
} */
function getUserById(id){
    return new Promise(function(resolve, reject){
        let query = 'select * from kullanici where kullaniciid = '+id;
        pool.query(query, function(err, user, fields){
            if(err) 
                reject(err);
            resolve(user.rows[0]);
        });
    });
}
function getAllRols(){
    return new Promise(function(resolve, reject){
        let query = 'select * from rol';
        pool.query(query, function(err, rols, fields){
            if(err) 
                reject(err);
            resolve(rols.rows);
        });
    }); 
}
function getAllCinsiyet(){
    return new Promise(function(resolve, reject){
        let query = 'select * from cinsiyet';
        pool.query(query, function(err, cinsiyet, fields){
            if(err) 
                reject(err);
            resolve(cinsiyet.rows);
        });
    }); 
}
function getAllCity(){
    return new Promise(function(resolve, reject){
        let query = 'select * from il';
        pool.query(query, function(err, cities, fields){
            if(err) 
                reject(err);
            resolve(cities.rows);
        });
    }); 
}
function getAllCounteres(){
    return new Promise(function(resolve, reject){
        let query = 'select * from uyruk';
        pool.query(query, function(err, counteres, fields){
            if(err) 
                reject(err);
            resolve(counteres.rows);
        });
    }); 
}
function getAllMeslek(){
    return new Promise(function(resolve, reject){
        let query = 'select * from meslek';
        pool.query(query, function(err, meslek, fields){
            if(err) 
                reject(err);
            resolve(meslek.rows);
        });
    }); 
}
module.exports = router;
